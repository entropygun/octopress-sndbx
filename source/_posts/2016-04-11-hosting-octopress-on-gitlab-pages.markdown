---
layout: post
title: "Hosting Octopress on GitLab Pages"
date: 2016-04-11 08:22:48 +0300
comments: true
categories: [GitLab, Pages]
---

 ![favicon](source/images/colophon-icon.png "favicon")  ~favicon~
 [link](source/images/colophon-icon.png)  

Welcome to Octopress!

This blog is hosted on [GitLab.com][] and deployed with [GitLab Pages][pages].

Read more at <https://gitlab.com/pages/octopress>.

Happy hacking!

[GitLab.com]: https://about.gitlab.com/gitlab-com
[pages]: https://pages.gitlab.io
